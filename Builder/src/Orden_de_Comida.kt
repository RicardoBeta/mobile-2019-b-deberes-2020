class Orden_de_Comida (val pan: String?,
                       val condimentos: String?,
                       val carne: String?,
                       val pescado: String?){
    data class Builder(
        var pan: String? = null,
        var condimentos: String? = null,
        var carne: String? = null,
        var pescado: String? = null) {

        fun pan(pan: String) = apply { this.pan = pan }
        fun condimentos(condimentos: String) = apply { this.condimentos = condimentos }
        fun carne(carne: String) = apply { this.carne = carne }
        fun pescado(pescado: String) = apply { this.pescado = pescado }
        fun build() = Orden_de_Comida(pan , condimentos, carne, pescado)


    }

    fun imprimir_Orden(){
        var orden : String =""
        if (this.pan != null)
            orden = orden +(" Pan : ${this.pan}\n")
        if (this.condimentos != null)
            orden= orden +(" Condimentos : ${this.condimentos}\n")
        if (this.carne != null)
            orden= orden +(" Carne : ${this.carne}\n")
        if (this.pescado != null)
            orden= orden +(" pescado : ${this.pescado}\n")
        println(orden)

    }

}