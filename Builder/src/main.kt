fun main(args: Array<String>) {
    val nueva_orden = Orden_de_Comida.Builder()
        .pan("Pan Integral")
        .carne("Carne de Cerdo")
        .condimentos("Pimientos")
        .build()
        .imprimir_Orden()
}