fun main(args: Array<String>) {
    val animalMaker: CloneFactory = CloneFactory()
    val sally: Sheep  = Sheep()
    val clonedSheep: Sheep = animalMaker.getClone(sally) as Sheep
    print(sally)
    print(clonedSheep)
}