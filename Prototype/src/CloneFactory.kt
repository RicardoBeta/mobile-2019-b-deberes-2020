class CloneFactory {
    fun getClone(animalSample: Animal): Animal{
        return animalSample.makecopy()
    }
}