package com.example.time_fighter

import android.content.Intent
import android.icu.text.CaseMap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class GameActivity : AppCompatActivity() {

    private lateinit var activityTitle: TextView
    private lateinit var playerName: String
    private lateinit var gameScoreTextView: TextView
    private lateinit var timeLeftTextView: TextView
    private lateinit var tapMeButton: Button
    private var gameScore = 0

    private lateinit var  countDownTimer: CountDownTimer
    private var inicialCountDown: Long = 10000
    private var countDownInterval : Long =1000
    private var timeLeft = 10
    private var isGameStarted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        activityTitle = findViewById(R.id.game_title)
        gameScoreTextView = findViewById(R.id.score_text_view)
        timeLeftTextView = findViewById(R.id.time_left_text_view)
        tapMeButton = findViewById(R.id.tap_me_button)
        playerName = intent.getStringExtra(MainActivity.INTENT_PLAYER_NAME) ?: "ERROR"
        activityTitle.text = getString(R.string.get_ready_player, playerName)
        tapMeButton.setOnClickListener{incrementScore()}
        resetGame()
    }


    private fun incrementScore(){

        if(!isGameStarted){
            startGame()
        }

        gameScore ++
        gameScoreTextView.text = getString(R.string.Puntos,gameScore)

    }

    private fun resetGame(){
        gameScore =0
        gameScoreTextView.text = getString(R.string.Puntos,gameScore)

        timeLeft=10
        timeLeftTextView.text = getString(R.string.Tiempo,timeLeft)


        countDownTimer = object: CountDownTimer(inicialCountDown,countDownInterval){
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt()/1000
                timeLeftTextView.text = getString(R.string.Tiempo,timeLeft)
            }



        }
        isGameStarted = false
    }
    private fun  startGame(){
        countDownTimer.start()
        isGameStarted = true

    }
    private fun endGame(){
        Toast.makeText(this,getString(R.string.Game_over,gameScore),Toast.LENGTH_LONG).show()
        resetGame()
    }
}
