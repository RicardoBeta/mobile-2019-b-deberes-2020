package com.example.time_fighter

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    lateinit var startButton: Button
    lateinit var playerName: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        playerName = findViewById(R.id.Player_name_input)
        startButton = findViewById(R.id.start_button)
        startButton.setOnClickListener{showGameActivity()}
    }


     private fun showGameActivity(){
        val gameActivityIntent = Intent(this,showGameActivity::class.java)
         gameActivityIntent.putExtra(INTENT_PLAYER_NAME,playerName.text.toString())
        startActivity(gameActivityIntent)
    }

    companion object {
        const val INTENT_PLAYER_NAME = "playerName"
    }
}