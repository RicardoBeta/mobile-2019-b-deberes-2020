package com.example.time_fighter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class showGameActivity : AppCompatActivity() {
    lateinit var activityTitle: TextView
    lateinit var playerName:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_game)
        activityTitle=findViewById(R.id.get_ready_player)
        playerName = intent.getStringExtra(MainActivity.INTENT_PLAYER_NAME)?:"ERROR"
        activityTitle.text = "Get Ready $playerName"
    }
}
