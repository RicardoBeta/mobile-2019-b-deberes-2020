interface ValueChangeListener {
    fun onValueChanged(newValue: String)
}