import kotlin.properties.Delegates
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class ObservableObject (listener: ValueChangeListener) {
    var text: String by Delegates.observable(
        initialValue = "",
        onChange = {
            prop, old, new ->
            listener.onValueChanged(new)
        })
}


