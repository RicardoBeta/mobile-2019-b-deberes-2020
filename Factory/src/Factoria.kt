import java.util.*

open class Factoria {
    fun getFactura(tipo:String):Factura{
        if (tipo.equals("iva")){
            return FacturaIVA()
        }else{
            return FacturaIVAreducido()
        }
    }
}