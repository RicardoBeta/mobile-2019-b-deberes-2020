class FacturaIVAreducido() : Factura() {
    override fun getImporteIVA(): Double {
        val ivaR = 1.06
        return getI().times(ivaR)
    }
}