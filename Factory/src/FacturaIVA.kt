class FacturaIVA : Factura() {
    override fun getImporteIVA(): Double {
        val iva = 1.12
        return getI().times(iva)
    }
}