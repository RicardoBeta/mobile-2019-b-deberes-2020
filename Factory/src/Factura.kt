abstract class Factura {
    var idF = 0
    var importe = 0.0

    fun getI():Double{
        return this.importe
    }

    fun getID():Int{
        return this.idF
    }
    fun setI(importe: Double){
        this.importe = importe
    }
    fun setID(idF: Int){
        this.idF = idF
    }

    abstract fun getImporteIVA(): Double
}