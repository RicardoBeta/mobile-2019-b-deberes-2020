abstract class Shape (var colorC: Color) {
    var color:Color
    init {
        this.color = colorC
    }
    abstract fun applyColor()
}