interface Beverage {
    var desription: String

    fun getDescription(): String = "Unknown beverage"

    fun cost(): Double
}