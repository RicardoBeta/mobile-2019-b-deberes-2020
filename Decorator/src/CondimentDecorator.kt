interface CondimentDecorator :Beverage {
    override fun getDescription(): String
}