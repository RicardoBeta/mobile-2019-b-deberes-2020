fun main(args: Array<String>) {
    println("Cafeteria del Barrio")
    var dr:Beverage = Espresso()
    dr= CocoaPowder(dr)
    println("""The cost of the beverage with ingredients:
        |${dr.getDescription()} has cost of: $${dr.cost()}""".trimMargin())
}