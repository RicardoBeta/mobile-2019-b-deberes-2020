class CocoaPowder(val beverage: Beverage): CondimentDecorator, Beverage by beverage {
    override fun cost(): Double {
        return beverage.cost() +  0.50
    }

    override fun getDescription(): String {
        return beverage.getDescription()+" and 15g of Cocoa Powder"
    }
}